Accounts.onCreateUser(function(options, user) {
    // Use provided profile in options, or create an empty object
    user.profile = options.profile || {};
    // Assigns custom fields to the newly created user object
    user.profile.team = options.team;
    user.profile.status = options.status;
    // Returns the user object
    return user;
});