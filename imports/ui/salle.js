import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session'

import { Réservations } from '../api/résas.js';
import { Salles } from '../api/résas.js';

import './salle.html';

Template.salle.onCreated(function salleOnCreated(){
    Meteor.subscribe('resas');
    Meteor.subscribe('salles');
    this.free = new ReactiveVar(true);
    this.date = new ReactiveVar("");
});

Template.salle.helpers({
    
});

Template.salle.events({
    'click .here'(event){
        const dates= Session.get("date");
        Template.instance().date.set(dates);
        var resa = Réservations.find({date: Template.instance().date.get()}).fetch();
        var salle = Salles.find().fetch();
        console.log(salle[5].info);
        
    },
});