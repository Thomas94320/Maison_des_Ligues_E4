import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';

import './espace.html';
import { Teams } from '../api/résas';

Template.espace.onCreated(function EspaceOnCreated(){
    Meteor.subscribe("users");
    Meteor.subscribe("teams");
    this.ligue = new ReactiveVar("");
});

Template.espace.helpers({
    IsAdmin: function() {
        return Meteor.user().profile.status=="admin";
      },

    IsModo: function() {
    return Meteor.user().profile.status=="modo";
    },
    IsPres: function() {
        return Meteor.user().profile.status=="pres";
    },
    members(){
        return Meteor.users.find({"profile.team": Meteor.user().profile.team});
    },
    ligues(){
        return Teams.find({});
    },
    membres(){
        return Meteor.users.find({"profile.team": Template.instance().ligue.get()});
    },
    admins(){
        return Meteor.users.find({"profile.status": {$in: [ "admin","modo" ]}});
    }
});

Template.espace.events({
    'submit .ligues'(event){
        event.preventDefault();
        Template.instance().ligue.set(event.target.ligue.value);
    }
});