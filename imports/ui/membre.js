import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';

import './membre.html';

Template.membre.onCreated(function MembreOnCreated(){    
    this.modify = new ReactiveVar(false);
});

Template.membre.helpers({
    Modify(){
        return Template.instance().modify.get();
    },
});

Template.membre.events({
    'click .modify'(){
        Template.instance().modify.set(true);
    },
    'click .cancel'(){
        Template.instance().modify.set(false);
    },

    'submit .confirm'(event){
        event.preventDefault();
        const statut = event.target.selector.value;
        Meteor.users.update({_id: this._id},{$set: {"profile.status": statut }});    
        Template.instance().modify.set(false);
    }
})