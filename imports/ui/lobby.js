import { Template } from 'meteor/templating';
import { Session } from 'meteor/session'
import { Meteor } from 'meteor/meteor';
 
import './lobby.html';

Template.lobby.onCreated(function LobbyOnCreated(){
    Session.get("signup");
});


Template.lobby.helpers({
    signUp(){
        return Session.get("signup");
    }
});