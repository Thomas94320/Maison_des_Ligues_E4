import { Template } from 'meteor/templating';
import { Session } from 'meteor/session'
import { Meteor } from 'meteor/meteor';
 
import './navbar.html';

Template.navbar.onCreated(function NavbarOnCreated(){
    Session.set("signup", false);
    Session.set("espace", false);
    Session.set("synthese", false);
});

Template.navbar.helpers({
    IsAdmin: function() {
        return Meteor.user().profile.status=="admin";
      },

    IsModo: function() {
    return Meteor.user().profile.status=="modo";
    },
    IsPres: function() {
        return Meteor.user().profile.status=="pres";
    }
    
});

Template.navbar.events({
    'click .logout': function(event){
        event.preventDefault();
        Meteor.logout();
    },
    'click .login': function(event){
        event.preventDefault();
        Session.set("signup", false);
    },
    'click .signup': function(event){
        event.preventDefault();
        Session.set("signup", true);
    },
    'click .admin': function(event){
        event.preventDefault();
        Session.set("espace", true);
    },
    'click .modo': function(event){
        event.preventDefault();
        Session.set("espace", true);
    },
    'click .synthese': function(event){
        event.preventDefault();
        Session.set("espace", false);
        Session.set("synthese", true);
        console.log(Session.get("synthese"));
    },
    'click .pres': function(event){
        event.preventDefault();
        Session.set("espace", true);
    },
    'click .navbar-brand': function(event){
        event.preventDefault();
        Session.set("espace", false);
        Session.set("synthese", false);
        console.log(Session.get("espace"));
    }
  });