import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import './lobby.js';
import './navbar.js';
import './selector.js';
import './résa.js';
import './salle.js';
import './categorie.js';
import './login.js';
import './register.js';
import './dashboard.js';
import './body.html';
import './espace.js';
import './membre.js';
import './admin.js';
import './synthese.js';

Template.body.onCreated(function BodyOnCreated(){
    Session.get("espace");
    Session.get("synthese");
});

Template.body.helpers({
    Espace(){
        return Session.get("espace") && (Meteor.user().profile.status=="admin" || Meteor.user().profile.status=="modo" || Meteor.user().profile.status=="pres");
    },
    Synthese(){
        return Session.get("synthese");
    }
});