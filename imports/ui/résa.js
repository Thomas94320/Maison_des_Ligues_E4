import { Template } from 'meteor/templating';

import { Meteor } from 'meteor/meteor';
 
import './résa.html';

Template.resa.onCreated(function resaOnCreated(){
    this.checked = new ReactiveVar(false);
    //this.motif = new ReactiveVar("");
});

Template.resa.helpers({
  isOwner() {
    return this.team == Meteor.user().profile.team;
  },
  Motif(){
    return Template.instance().checked.get();
  }
});
 
Template.resa.events({
  
  'click .delete'() {
    if(this.team ==  Meteor.user().profile.team && Meteor.user().profile.status !== "membre" || Meteor.user().profile.status == "admin" || Meteor.user().profile.status == "modo"){ //TODO add admin autorisation
      Template.instance().checked.set(true);
      
    }
    else{
      swal("Action impossible");
    }
    
  },
  'submit .annuler'(event){
    event.preventDefault();
    //Template.instance().motif.set(document.getElementById("motif").value);
    var motif = document.getElementById("motif").value;
    Meteor.call('resas.remove', this._id,motif);
    Template.instance().checked.set(false);
}
});