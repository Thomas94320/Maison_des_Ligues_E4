import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Session } from 'meteor/session'
import swal from 'sweetalert';

import { Réservations } from '../api/résas.js';
import { Salles } from '../api/résas.js';
import { Categories } from '../api/résas.js';
import { Rooms } from '../api/résas.js';
import { Horaires } from '../api/résas.js';

import './dashboard.html';

Template.dashboard.onCreated(function DashboardOnCreated(){
    Meteor.subscribe('resas');
    Meteor.subscribe('salles');
    Meteor.subscribe('cats');
    Meteor.subscribe('rooms');
    Meteor.subscribe('horaires');

    this.checked = new ReactiveVar(false);
    this.date = new ReactiveVar("");
    this.cat = new ReactiveVar("");
    this.num = new ReactiveVar("");
    this.cpt = new ReactiveVar(true);
    this.hour = new ReactiveVar("");
    this.volume = new ReactiveVar("");
});

Template.dashboard.helpers({
    resas() {
        return Réservations.find({});
      },
    salles(){             
        return Rooms.find({cat:Template.instance().cat.get()});     
    },
    cats(){
      return Categories.find({});
    },
    IsChecked: function() {
      return Template.instance().checked.get();
    },
    hours(){
      return Horaires.find({});
    }
});

Template.dashboard.events({
    'submit .new-submit'(event){

      if(Meteor.user().profile.status !== "membre" && Meteor.user().profile.status !== "admin" && Meteor.user().profile.status !== "modo"){
      event.preventDefault();
      Template.instance().cpt.set(true);
      Template.instance().checked.set(true);

      const target = event.target;
      const date = target.date.value;
      const cat = target.cat.value;
      const hour = target.plage.value;
      const volume = target.selector.value;

      Template.instance().cat.set(cat);
      Template.instance().volume.set(volume);
      Template.instance().date.set(date);
      Template.instance().hour.set(hour);
      Session.set("date", date);
      Session.set("hour", hour);
      
      Meteor.call('rooms.insert',cat);//Liste des salles de la bonne cat
      
      Template.instance().num.set(Réservations.find({date: Template.instance().date.get()}).map(function (person) { return person.num; }));
      var resa = Réservations.find({
        $and: [
          {date: Template.instance().date.get()},
          {hour: Template.instance().hour.get()}
      ]}).map(function (person) { return person.num; });//toutes les num réservations à la date choisie et à l'heure

      //console.log(Template.instance().volume.get());

      var salle = Rooms.find({cat: Template.instance().cat.get()}).fetch();// toutes les salles de la bonne cat

      for (i=0;i<resa.length;i++){
        for(j=0;j<salle.length;j++){
          //console.log(salle[j].volume+ " "+volume);          
            if(salle[j].num == resa[i]){//all rooms reservées
              //console.log("Salle "+salle[j].num+ " "+Template.instance().cat.get()+ " est occupée");
              Rooms.remove(salle[j]._id);              
            }
        }
      }
      salle = Rooms.find().fetch();
      for(i=0;i<salle.length;i++){
        if (salle[i].volume<=Template.instance().volume.get()){
          Rooms.remove(salle[i]._id);
        }
      }
      if(Rooms.find().count()<=0){
        Template.instance().checked.set(false);
        swal('Aucune salle ne correspond à vos critères');
      }
    }
    else{
      swal("Impossible d'effectuer cette action");
    }
     

    },
    'submit .new-resa'(event) {
      // Prevent default browser form submit
      event.preventDefault();
   
      // Get value from form element
      const target = event.target;
      const date = Template.instance().date.get();
      const num= target.salle.value;
      const hour = Template.instance().hour.get();
      const motif = "";
      const statut = "Effectuée";

      Meteor.call('resas.insert',date, num, hour,motif,statut);
      // Clear form
      target.salle.value = '';
      Template.instance().checked.set(false);
      Template.instance().date.set("");
      Template.instance().hour.set("");
      Template.instance().volume.set("");
    }
  });