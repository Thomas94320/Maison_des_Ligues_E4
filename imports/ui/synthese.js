import { Template } from 'meteor/templating';
import { Session } from 'meteor/session'
import { Meteor } from 'meteor/meteor';
 
import './synthese.html';

import { Réservations } from '../api/résas.js';

Template.synthese.onCreated(function syntheseOnCreated(){
    Meteor.subscribe('resas');
});

Template.synthese.helpers({
    res(){
        return  Réservations.find({statut: "Effectuée"});
    },
    nb: function () {
        return Réservations.find({statut: "Effectuée"}).count();
    },
    res2(){
        return  Réservations.find({statut: "Annulée"});
    },
    res1(){
        return  Réservations.find({statut: "Déplacée"});
    },
    nb1: function () {
        return Réservations.find({statut: "Déplacée"}).count();
    },
    nb2: function () {
        return Réservations.find({statut: "Annulée"}).count();
    },
});

