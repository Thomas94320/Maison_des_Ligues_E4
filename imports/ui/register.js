import { Template } from 'meteor/templating';

import { Meteor } from 'meteor/meteor';
 
import './register.html';

import { Teams } from '../api/résas.js';
import swal from 'sweetalert';

Template.register.onCreated(function RegisterOnCreated(){
    Meteor.subscribe('teams');

});

Template.register.helpers({
    teams(){
        return Teams.find({});
    },
});

if (Meteor.isClient) {
    Template.register.events({
        'submit form': function(event) {
            event.preventDefault();
            var email = event.target.email.value;
            var password = event.target.password.value;
            var team = event.target.team.value;
            var username = event.target.username.value;
            var status = "membre";
            if (username.length>25){
                swal('Username trop long');
            }
            else{
                Accounts.createUser({
                    email,
                    password,
                    team,
                    username,
                    status
                });
            }
            
        }
    });
}