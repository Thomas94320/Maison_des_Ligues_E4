import { Template } from 'meteor/templating';

import { Meteor } from 'meteor/meteor';
 
import './login.html';

Template.login.events({
    'submit form': function(event) {
        event.preventDefault();
        var emailVar = event.target.username.value;
        var passwordVar = event.target.password.value;
        Meteor.loginWithPassword(emailVar, passwordVar, function(error) {
            if (error) {
                swal({
                   title: "Username/Email or password incorrect",
                   icon: "error"
               });
            }
        });
    }
});