import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';

import './admin.html';

Template.admin.onCreated(function AdminOnCreated(){    
    this.modify = new ReactiveVar(false);
});

Template.admin.helpers({
    Modify(){
        return Template.instance().modify.get();
    }
});

Template.admin.events({
    'click .modify'(){
        Template.instance().modify.set(true);
    },
    'click .cancel'(){
        Template.instance().modify.set(false);
    },

    'submit .confirm'(event){
        event.preventDefault();
        const statut = event.target.selector.value;
        Meteor.users.update({_id: this._id},{$set: {"profile.status": statut }});    
        Template.instance().modify.set(false);
    }
})