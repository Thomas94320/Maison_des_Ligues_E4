import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import swal from 'sweetalert';
 
export const Réservations = new Mongo.Collection('resas');
export const Salles = new Mongo.Collection('salles');
export const Categories = new Mongo.Collection('cats');
export const Rooms = new Mongo.Collection('rooms');
export const Horaires = new Mongo.Collection('horaires');
export const Teams = new Mongo.Collection('teams');

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish('resas', function resasPublication() {
    return Réservations.find();
  });
  Meteor.publish('salles', function resasPublication() {
    return Salles.find();
  });
  Meteor.publish('cats', function resasPublication() {
    return Categories.find();
  });

  Meteor.publish('rooms', function resasPublication() {
    return Rooms.find();
  });

  Meteor.publish('horaires', function resasPublication() {
    return Horaires.find();
  });
  Meteor.publish('teams', function resasPublication() {
    return Teams.find();
  });

  Meteor.publish("users", function () {
    return Meteor.users.find({});
  });

  Meteor.users.allow({
      'update': function(userId,doc){
        return true;
      }
  });

  Réservations.allow({
    'insert': function (userId,doc) {
      /* user and doc checks ,
      return true to allow insert */
      return true; 
    }
  });
  Réservations.allow({
    'update': function (userId,doc) {
      /* user and doc checks ,
      return true to allow insert */
      return true; 
    }
  });
  Salles.allow({
    'insert': function (userId,doc) {
      /* user and doc checks ,
      return true to allow insert */
      return true; 
    }
  });
  Categories.allow({
    'insert': function (userId,doc) {
      /* user and doc checks ,
      return true to allow insert */
      return true; 
    }
  });
  Rooms.allow({
    'insert': function (userId,doc) {
      /* user and doc checks ,
      return true to allow insert */
      return true;
      }
  });

  Rooms.allow({
    'remove': function (userId,doc) {
      /* user and doc checks ,
      return true to allow insert */
      return true;
      }
  })
}

Meteor.methods({
  'resas.insert'(date, num, hour,motif,statut) {
    check(date, String);
    check(num, String);
    check(motif, String);
    check(statut, String);
    team = Meteor.user().profile.team;
    
    Réservations.insert({
      date,
      num,
      createdAt: new Date(),
      team,
      hour,
      motif,
      statut
    });
  },
  'resas.remove'(resaId, motif) {
    check(resaId, String);
    check(motif, String);
    Réservations.update({_id : resaId},{$set:{statut : "Annulée", motif:motif}});
  },
  /*'resas.setChecked'(resaId, setChecked) {
    check(resaId, String);
    check(setChecked, Boolean);
 
    Réservations.update(resaId, { $set: { checked: setChecked } });
  },*/
  'rooms.insert'(cat){
    Rooms.remove({});

    
    Salles.find({cat: cat}).map(function (person) { 
      Rooms.insert({
        num: person.num,
        cat: person.cat,
        volume: person.volume,
        info: person.info
      }) 
    })
    
  },

});